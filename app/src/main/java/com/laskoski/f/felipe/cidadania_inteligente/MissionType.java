package com.laskoski.f.felipe.cidadania_inteligente;

/**
 * Created by Felipe on 12/3/2017.
 */

public enum MissionType {
    QUESTION, QUIZZ, CITY_ISSUE;
}
